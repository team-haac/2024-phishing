# 2024-Phishing

## Projects

- Backend - a flask app  
  `cd backend` and read instructions

- Frontend - a nextjs app
  `cd frontend` and read instructions

## Running Demo

This can be run using Docker:

```sh
docker compose up --build
```

Then head over to http://localhost:3000
