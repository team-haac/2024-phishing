# Frontend

Built with Next.js & React. Bun is the package manager.

## How to Run

```sh
# Install Bun
curl -fsSL https://bun.sh/install | bash
bun -v
# Make sure you have Node.js installed
node -v
# Run Dev Server
bun dev
```
