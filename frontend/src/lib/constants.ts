export const API_URL = URL.canParse(process.env["API_URL"] ?? "")
  ? process.env["API_URL"]!
  : "http://localhost:8000";
