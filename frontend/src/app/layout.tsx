import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { Toaster } from "@/components/ui/toaster";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "PhishScore",
  description: "PhishScore is a phishing detection tool by Team HAAC",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <head>
        <link rel="icon" type="image/png" href="/smallfish.png" />
      </head>
      <body className={inter.className}>
        <div className="min-h-screen w-screen">{children}</div>
        <Toaster />
      </body>
    </html>
  );
}
