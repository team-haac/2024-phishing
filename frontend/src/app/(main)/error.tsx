"use client";

import {
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";

export default function Error() {
  return (
    <>
      <CardHeader>
        <CardTitle>Error</CardTitle>
        <CardDescription>Failed to fetch data</CardDescription>
      </CardHeader>
      <CardContent>Reload the page to retry</CardContent>
    </>
  );
}
