import { Card } from "@/components/ui/card";

export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <main className="min-h-screen w-screen p-8">
      <Card className="min-h-full w-full">{children}</Card>
    </main>
  );
}
