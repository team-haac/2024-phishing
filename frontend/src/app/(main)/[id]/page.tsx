import { MoveLeftIcon } from "lucide-react";
import NextLink from "next/link";
import { notFound } from "next/navigation";

import {
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Textarea } from "@/components/ui/textarea";
import { API_URL } from "@/lib/constants";
import { oneEmailDataResponse } from "../stuff";

export default async function Home({
  params: { id },
}: {
  params: { id: string };
}) {
  const res = await fetch(`${API_URL}/one/${id}`);
  const data = (await res.json()) as unknown;

  const email = oneEmailDataResponse.parse(data);
  if (!email.submission) {
    return notFound();
  }

  return (
    <>
      <CardHeader>
        <CardTitle>View Submission</CardTitle>
        <CardDescription>
          <NextLink
            href={"/"}
            className="inline-text inline-flex items-center font-semibold hover:underline"
          >
            <MoveLeftIcon className="mr-0.5 h-6 w-auto" /> Go Back
          </NextLink>
        </CardDescription>
      </CardHeader>
      <CardContent>
        <div className="grid w-full items-center gap-4">
          <div className="flex w-full items-center justify-between gap-x-2">
            <div className="flex w-full grow flex-col space-y-1.5">
              <Label>Submitter</Label>
              <Input
                type="text"
                disabled={true}
                value={email.submission.submitter}
                className=" disabled:opacity-100"
              />
            </div>
            <div className="flex flex-col space-y-1.5 justify-self-end">
              <Label>Score</Label>
              <Input
                type="text"
                disabled={true}
                value={email.submission.score}
                className=" disabled:opacity-100"
              />
            </div>
          </div>
          <div className="flex flex-col space-y-1.5">
            <Label>Email Address</Label>
            <Input
              type="text"
              disabled={true}
              value={email.submission.email}
              className=" disabled:opacity-100"
            />
          </div>
          <div className="flex flex-col space-y-1.5">
            <Label>Email Subject</Label>
            <Input
              type="text"
              disabled={true}
              value={email.submission.subject}
              className=" disabled:opacity-100"
            />
          </div>
          <div className="flex flex-col space-y-1.5">
            <Label>Email Body</Label>
            <Textarea
              disabled={true}
              value={email.submission.body}
              className=" disabled:opacity-100"
            />
          </div>
        </div>
      </CardContent>
    </>
  );
}
