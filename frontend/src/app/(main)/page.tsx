import { MoveRight } from "lucide-react";
import NextLink from "next/link";

import {
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { API_URL } from "@/lib/constants";
import { allEmailData } from "./stuff";

export const dynamic = "force-dynamic";

export default async function Home() {
  const res = await fetch(`${API_URL}/all`, {
    cache: "no-store",
  });
  const data = (await res.json()) as unknown;

  const allEmails = allEmailData.parse(data);
  if (!allEmails.submissions) {
    return <div>Failed to fetch data</div>;
  }

  return (
    <>
      <CardHeader>
        <CardTitle>All Submissions</CardTitle>
        <CardDescription>
          Here is a list of all the subimissions. Click one to view more
          information. Reload if needed. <br />
          <NextLink
            href={"/submit"}
            className="inline-text inline-flex items-center font-semibold hover:underline"
          >
            Submit <MoveRight className="ml-0.5 h-auto w-6" />
          </NextLink>
        </CardDescription>
      </CardHeader>
      <CardContent>
        <Table className="w-full">
          <TableHeader>
            <TableRow className="w-full">
              <TableHead className=" w-16">Score</TableHead>
              <TableHead className="">From</TableHead>
              <TableHead className="">Subject</TableHead>
              <TableHead className="w-12">
                <span className="sr-only">View</span>
              </TableHead>
            </TableRow>
          </TableHeader>
          <TableBody>
            {Object.entries(allEmails.submissions).map(([id, email]) => {
              return (
                <TableRow
                  key={id}
                  className="w-full group max-w-full overflow-x-clip"
                >
                  <TableCell className="w-16 text-center">
                    {email.score}
                  </TableCell>
                  <TableCell className="max-w-44">
                    <p className="w-full truncate group-hover:overflow-x-scroll group-hover:py-3">
                      {email.email}
                    </p>
                  </TableCell>
                  <TableCell className="w-full">{email.subject}</TableCell>
                  <TableCell className="w-12">
                    <NextLink
                      key={id}
                      href={`/${id}`}
                      title="Click to view!"
                      className="inline-text inline-flex items-center font-semibold hover:underline"
                    >
                      View <MoveRight className="ml-0.5 h-auto w-6" />
                    </NextLink>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </CardContent>
    </>
  );
}
