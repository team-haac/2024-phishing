import { z } from "zod";

export const emailData = z.object({
  email: z.string(),
  subject: z.string(),
  body: z.string(),
  score: z.number(),
});

export const oneEmailDataResponse = z
  .object({
    submission: emailData.merge(z.object({ submitter: z.string() })),
  })
  .or(
    z.object({
      submission: z.null(),
    })
  );

export const allEmailData = z
  .object({
    submissions: z.record(z.string().uuid(), emailData),
  })
  .or(z.object({ submissions: z.null() }));
