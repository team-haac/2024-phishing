import {
  Card,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { SubmitForm } from "./submitform";

export default function Home() {
  return (
    <main className="min-h-screen w-screen px-8 pt-12 lg:px-0">
      <Card className="mx-auto min-h-0 w-full md:w-8/12 lg:w-5/12">
        <CardHeader>
          <CardTitle>Submit an email</CardTitle>
          <CardDescription>
            Submit a phishing email to be analyzed by our system
          </CardDescription>
        </CardHeader>
        <SubmitForm />
      </Card>
    </main>
  );
}
