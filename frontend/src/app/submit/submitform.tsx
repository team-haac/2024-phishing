"use client";

import { LucideLoader2 } from "lucide-react";
import { useRouter } from "next/navigation";
import { useCallback, useState } from "react";
import { z, ZodError } from "zod";

import { Button } from "@/components/ui/button";
import { CardContent, CardFooter } from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Textarea } from "@/components/ui/textarea";
import { useToast } from "@/components/ui/use-toast";

const emailSubmitSchema = z.object({
  submitterName: z.string(),
  email: z.string().email(),
  subject: z.string().min(1).max(100),
  body: z.string().min(1).max(2500),
});

export function SubmitForm() {
  const [loading, setLoading] = useState(false);
  const [formState, setFormState] = useState<z.infer<typeof emailSubmitSchema>>(
    { subject: "", body: "", email: "", submitterName: "" }
  );

  const router = useRouter();

  const { toast } = useToast();

  const getFormValues = useCallback(() => {
    return formState;
  }, [formState]);

  const handleSubmit = useCallback(
    async (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      setLoading(true);

      const formValues = getFormValues();
      console.log({ formValues });

      let data;
      try {
        data = emailSubmitSchema.parse(formValues);
      } catch (e: unknown) {
        console.error(e);
        if (e instanceof ZodError) {
          toast({
            variant: "destructive",
            title: "Invalid Submission",
            description: e.errors.map((error) => error.message).join("\n"),
          });
        }
        setLoading(false);
        return;
      }

      let id = "";

      try {
        // Request gets rewritten in API
        const res = await fetch(`/api/submit`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        });

        // wooooooooo this is a bad idea
        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
        id = (await res.json()).id as unknown as string;

        if (typeof id !== "string") {
          throw new Error();
        }

        console.log({ id });
      } catch (e) {
        console.error(e);
        toast({
          variant: "destructive",
          title: "Submission Failed",
          description:
            "There was an error submitting your email. Make sure the server is running.",
        });
        setLoading(false);
        return;
      }

      toast({
        title: "Email Submitted",
        description: "Your email has been submitted successfully",
      });

      router.push(`/${id}`);
      setLoading(false);
    },
    [getFormValues, router, toast]
  );

  return (
    <>
      <CardContent className="w-full">
        <form onSubmit={handleSubmit} id="emailSubmit">
          <div className="grid w-full items-center gap-4">
            <div className="flex flex-col space-y-1.5">
              <Label htmlFor="submitterName">Submitter Name</Label>
              <Input
                type="text"
                id="submitterName"
                placeholder="Your name"
                minLength={1}
                disabled={loading}
                value={formState.submitterName}
                onChange={(e) =>
                  setFormState({
                    ...formState,
                    submitterName: e.target.value,
                  })
                }
                autoComplete="off"
              />
            </div>
            <div className="flex flex-col space-y-1.5">
              <Label htmlFor="email">Email Address</Label>
              <Input
                type="text"
                id="email"
                placeholder="The spammer's email"
                minLength={2}
                disabled={loading}
                value={formState.email}
                onChange={(e) =>
                  setFormState({ ...formState, email: e.target.value })
                }
                autoComplete="off"
              />
            </div>
            <div className="flex flex-col space-y-1.5">
              <Label htmlFor="subject">Email Subject</Label>
              <Input
                type="text"
                id="subject"
                placeholder="The email subject line"
                minLength={1}
                disabled={loading}
                value={formState.subject}
                onChange={(e) =>
                  setFormState({ ...formState, subject: e.target.value })
                }
                autoComplete="off"
              />
            </div>
            <div className="flex flex-col space-y-1.5">
              <Label htmlFor="body">Email Body</Label>
              <Textarea
                id="body"
                placeholder="The email contents"
                minLength={1}
                className=" min-h-32"
                disabled={loading}
                value={formState.body}
                onChange={(e) =>
                  setFormState({ ...formState, body: e.target.value })
                }
                autoComplete="off"
              />
            </div>
          </div>
        </form>
      </CardContent>
      <CardFooter className="flex justify-between">
        <Button
          type="submit"
          form="emailSubmit"
          className="self-end"
          disabled={loading}
        >
          {loading ? (
            <>
              <LucideLoader2 className="size-6 animate-spin" />
            </>
          ) : (
            "Submit"
          )}
        </Button>
      </CardFooter>
    </>
  );
}
