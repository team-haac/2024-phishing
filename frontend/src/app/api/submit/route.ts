import { API_URL } from "@/lib/constants";

async function post(req: Request) {
  const json = (await req.json()) as unknown;
  // console.log({ json, API_URL });

  try {
    const serverRes = await fetch(new URL("/submit", API_URL), {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(json),
      cache: "no-store",
    });

    // console.log("Submit Redirect Successful", { serverRes });

    return serverRes;
  } catch (e) {
    console.error("Submit Redirect Failed", e);
    return Response.json({ id: null, success: false });
  }
}

export { post as POST };
