// @ts-check
/** @type {import('eslint').Linter.Config} */
module.exports = {
  root: true,
  extends: ["henrik/next"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: "tsconfig.json",
    tsconfigRootDir: __dirname,
  },
  rules: {
    "tailwindcss/classnames-order": 0,
    "drizzle/enforce-delete-with-where": 0,
  },

  // rest of your config
};
