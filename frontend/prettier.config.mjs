import sharedConfig from "prettier-config-henrik";

/** @type {import('@ianvs/prettier-plugin-sort-imports').PrettierConfig} */
const config = {
  ...sharedConfig,
  plugins: ["@ianvs/prettier-plugin-sort-imports"],

  // your config here
  importOrderTypeScriptVersion: "5.4.5",
};

export default config;
