def cast_line(dictionary, phish_words):
    found_words = []
    phish_score = 0
    for word in phish_words:
        if word in dictionary.keys():
            found_words.append(word)
            phish_score += dictionary[word]
    phish_score = phish_score * len(found_words)
    return phish_score

# Case insensitively checks words
if __name__ == '__main__':
    inputs = input("Enter a string: ").lower()
    phish_words = ["emergency", "important", "urgent"]

    found_words = cast_line(inputs, phish_words)

    if found_words:
        if len(found_words) > 2:
            print("Smells like phish.")
    else:
        print("Looks good!")
