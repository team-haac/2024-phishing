def process_data(read_data):
    processed_data = {}
    for word in read_data.split():
        if word not in processed_data.keys():
            processed_data[word] = 1
        else:
            processed_data[word] = processed_data[word] + 1
    return processed_data

if __name__ == '__main__':
    d = {1: "the",
         2: "the",
         3: "the",
         4: "a",
         5: "a",
         6: "one",
         7: "AndresH",
         8: "Honor",
         9: "Society",
         10: "urgent",
         11: "instant",
         12: "Jackie",
         13: "Clayton",
         14: "Edwina",
         15: "Eggbert",
         16: "Clayton",
         17: "Clayton",
         18: "donde",
         19: "esta",
         20: "biblioteca"}
    data = process_data(d)
    print(data)
        
