import json
import uuid

data = {}

def dump():
  with open('data.json', 'w') as f:
    json.dump(data, f)

def load():
  global data
  try:
    with open('data.json', 'r') as f:
      data = json.load(f)
  except FileNotFoundError:
    print("File not found. Creating new file.")
    data = {
      "submissions": {}
    }
    dump()
    
    
load()

def submit(submitter: str, email: str, subject: str, body: str) -> str:
  id = str(uuid.uuid4())
  submission = {
    "submitter": submitter,
    "email": email,
    "subject": subject,
    "body": body,
    "score": -1
  }
  
  data['submissions'][id] = submission
  dump()
  return id

def score_submission(id: str, score: int) -> bool:
  '''Score a submission by id. Returns True if the submission was found and scored, False otherwise.'''
  try:
    data['submissions'][id]['score'] = score
    dump()
    return True
  except KeyError:
    return False

def get_submissions():
  """Get all submissions."""
  load()
  return data['submissions']

def get_submission(id: str) -> dict[str, str]: 
  """Get a submission by id. Returns None if the submission was not found."""
  load()
  try:
    return data['submissions'][id]
  except KeyError:
    return None


