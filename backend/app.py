from flask import Flask, jsonify, request
from flask_cors import CORS
import db
import process_data
import phishing_rod

PHISH_WORDS = ["urgent",
"verify", 
"account",
"confirm", 
"identity",
"immediate",
"action",
"required",
"suspension",
"offer",
"compromised",
"alert",
"unusual",
"activity",
"click",
"free",
"money",
"prize",
"update", 
"information",
"password",
"reset",
"deleted",
"suspicious",
"login",
"security",
"notice",
"hacked",
"warning",
"closed",
"unauthorized",
"access",
"detected",
"claim",
"attention",
"important",
"limited",
"final",
"reminder",
"avoid",
"closure",
"verification",
"purchase",
"reward",
"suspended",
"issue",
"immediately",
"risk",
"prevent",
"payment",
"expired",
"critical",
"lockout",
"reset"
"bonus",
"jeopardy",
"status",
"low",
"settings",
"subscription",
"billing",
"ownership",
"resolve",
"profile",
"feedback",
"flagged",
"preferences",
"attempt",
"expiring",
"secure",
"communication",
"investigation",
"shipping"]

app = Flask(__name__)

cors = CORS(app)

@app.route("/")
def hello():
    return "Hello, World!"

@app.route("/all",  methods=['GET'])
def get_all():
    data = db.get_submissions()
    if data is None:
        return jsonify({ "submissions": {}, "success": False })
    return jsonify({ "submissions": data, "success": True })

@app.route("/one/<id>",  methods=['GET'])
def get_submission(id):
    data = db.get_submission(id)
    if data is None:
        return jsonify({ "submission": None, "success": False })
    return jsonify({ "submission": data, "success": True })

@app.route("/submit",  methods=['POST'])
def submit():
    data = request.get_json()
    
    emailAddress = data['email']
    subject = data['subject']
    submitter = data['submitterName']
    emailBody = data['body']
    
    if not emailAddress or not subject or not submitter or not emailBody:
        return jsonify({ "id": None, "success": False })
    
    print(f"Email Address: {emailAddress}")
    print(f"Subject: {subject}")
    print(f"Submitter: {submitter}")
    print(f"Email Body: {emailBody}")

    id_from_submission = db.submit(submitter, emailAddress, subject, emailBody)
    bait = process_data.process_data(emailBody) # Returns the occurences of each word in the given string
    dinner = phishing_rod.cast_line(bait, PHISH_WORDS) # Returns the phishiness score
    db.score_submission(id_from_submission, dinner)
    return jsonify({ "id": id_from_submission, "success": True })
