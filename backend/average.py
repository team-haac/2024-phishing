PHISH_WORDS = ["urgent",
"verify", 
"account",
"confirm", 
"identity",
"immediate",
"action",
"required",
"suspension",
"offer",
"compromised",
"alert",
"unusual",
"activity",
"click",
"free",
"money",
"prize",
"update", 
"information",
"password",
"reset",
"deleted",
"suspicious",
"login",
"security",
"notice",
"hacked",
"warning",
"closed",
"unauthorized",
"access",
"detected",
"claim",
"attention",
"important",
"limited",
"final",
"reminder",
"avoid",
"closure",
"verification",
"purchase",
"reward",
"suspended",
"issue",
"immediately",
"risk",
"prevent",
"payment",
"expired",
"critical",
"lockout",
"reset"
"bonus",
"jeopardy",
"status",
"low",
"settings",
"subscription",
"billing",
"ownership",
"resolve",
"profile",
"feedback",
"flagged",
"preferences",
"attempt",
"expiring",
"secure",
"communication",
"investigation",
"shipping"]

import process_data, phishing_rod
avg = []
for i in range(11):
    file_name = input("Enter a File Name")
    with open("sample/" + file_name, "r"):
        bait = process_data.process_data() # Returns the occurences of each word in the given string
        dinner = phishing_rod.cast_line(bait, PHISH_WORDS) # Returns the phishiness score
        avg.append(dinner)

sum = 0
for e in avg:
    sum = sum + e
print(sum / len(avg))
    