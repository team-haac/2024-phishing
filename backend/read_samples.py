def parse_file(raw: str):
  """Returns a tuple with the format (from, subject, content)"""
  lines = raw.split("---")
  return (lines[0], lines[1], lines[2])
